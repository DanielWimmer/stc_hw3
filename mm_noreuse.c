#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
   int i, j, k;
   int array_size = 2048;

   double A[array_size][array_size];
   double B[array_size][array_size];
   double C[array_size][array_size];

   for(i=0;i<array_size;i++)           // Fill arrays A and B with values of 1.0
   {
      for(j=0;j<array_size;j++)
         {
            A[i][j] = 1.0;
            B[i][j] = 1.0;
         }
   }

   for(i=0;i<array_size;i++)
   {
      for(j=0;j<array_size;j++)
      {  
         for(k=0;k<array_size;k++)
         {
            C[i][j] = C[i][j] + A[i][k] * B[k][j];
         }
      }
   }

   return 0;
}
